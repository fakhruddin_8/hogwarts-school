using CourseAPI.Models;
using CourseAPI.Services;
using Microsoft.AspNetCore.Mvc;

namespace CourseAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CourseController:ControllerBase
    {
        public ICourseService _ICourseService;
        public CourseController(ICourseService ICourseService)
        {
        _ICourseService=ICourseService;
        }

        [HttpPost("NewCourse")]
        public ActionResult Post(Course course)
        {bool flag=this._ICourseService.AddCourses(course);
                if(flag==true)
                {
                    return Ok(" Course Added");
                }else{
                return BadRequest("Please Give appropriate felds");
                    }
        }

         [HttpGet("GetCourse/{id}")]
         public ActionResult Get(int id)
         {var staff =this._ICourseService.GetbyId(id);
            if (staff==null)
            {
               return Ok("No Records");
            }else{return Ok(this._ICourseService.GetbyId(id));}
            
         }
    }
}