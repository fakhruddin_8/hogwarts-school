using System;
using System.Collections.Generic;
using System.Linq;
using CourseAPI.Models;

namespace CourseAPI.Services
{
    public class CourseService:ICourseService
    {
        public CourseContext _CourseContext;
        public CourseService(CourseContext CourseContext)
        {
            _CourseContext=CourseContext;
        }
        public List<Course> GetCourses()
        {  try
               {
                return _CourseContext.Courses.ToList();}
                catch(Exception ex)
                    {
                     throw ex;}
        }
        public bool AddCourses(Course course)
        {  try{
                _CourseContext.Courses.Add(course);
                _CourseContext.SaveChanges();
                return true;}
                catch(Exception ex)
                {
                 Console.WriteLine(ex);
                 return false;
                }
        }

        public Course GetbyId (int id)
        {try{
                    var course =_CourseContext.Courses.Find(id);
                    return course;}
                 catch(Exception ex)
                    {
                    throw ex;}
        }
    }
}