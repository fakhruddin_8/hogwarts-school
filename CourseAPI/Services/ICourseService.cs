using System.Collections.Generic;
using CourseAPI.Models;

namespace CourseAPI.Services
{
    public interface ICourseService
    {
        List<Course> GetCourses();
        bool AddCourses(Course course);
        Course GetbyId(int id);
    }
}