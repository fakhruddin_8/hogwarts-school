using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;

namespace CourseAPI.Models
{
    public class Course
    {
        [Key]
        public int CourseId { get; set; }

        [Required]
        [Column(TypeName="varchar(30)")]
        public string CourseName { get; set; }

        [Column(TypeName="varchar(100)")]
        public string CoursePreRequisites { get; set; }

        [Column(TypeName="varchar(500)")]
        public string CourseDescription { get; set; }

        [Column(TypeName="int")][Range(0,5)]
        public int Credits { get; set; }

    }
}