using System;
using System.Collections.Generic;
using System.Linq;
using UserAPI.Models;

namespace UserAPI.Services
{
    public class StudentService:IStudentService
    {  
        public UserContext _Studentlist;
        public StudentService(UserContext Studentlist)
        {
            _Studentlist=Studentlist;
        }
        public List<Student> GetStudents() 
       {   try
               {
                return _Studentlist.Students.ToList();}
                catch(Exception ex)
                    {
                     throw ex;}
       }
        public bool AddStudent(Student student)
        {   try{
                _Studentlist.Students.Add(student);
                _Studentlist.SaveChanges();
                return true;}
                catch(Exception ex)
                {
                 Console.WriteLine(ex);
                 return false;
                }
        }   
        public Student GetbyId(int id)  
        {try{
                    var student =_Studentlist.Students.Find(id);
                    return student;}
                 catch(Exception ex)
                    {
                    throw ex;}
        }  
        public bool UpStud(int id, Student st)
        {try{
             var student =_Studentlist.Students.Find(id);
             student.FirstName=st.FirstName;
             student.LastName=st.LastName;               
             student.FatherName=st.FatherName;
             student.DateOfBirth=st.DateOfBirth;
             student.Address=st.Address;
             student.ContactNumber=st.ContactNumber;
             student.Gender=st.Gender;
             _Studentlist.SaveChanges();
             return true;}
             // _Stafflist.SaveChanges();}
             catch(Exception ex)
             {
              Console.WriteLine(ex);
              return false;
              }
        }

        public  bool PartialUpStud(int id, Student st)
        {try{ 
            var student =_Studentlist.Students.Find(id);
            student.Address=st.Address;
            student.ContactNumber=st.ContactNumber;
                    _Studentlist.SaveChanges();
                    return true;}
                    catch(Exception ex)
                        {
                            Console.WriteLine(ex);
                            
                            return false;
                        }
        }
        public bool DelStudent(int id)
        {try{
                var p=_Studentlist.Students.Find(id);
                //    p.FirstName = string.Empty;
                //     p.LastName = string.Empty;
                //     p.DateOfBirth = DateTime.MinValue;
                //     p.ContactNumber = string.Empty;
                //     p.Address = string.Empty;
                //     p.Gender = string.Empty;
                //     p.FatherName = string.Empty;
              _Studentlist.Remove(p);
              _Studentlist.SaveChanges();
                    return true;}
                    catch(Exception ex)
                        {
                            Console.WriteLine(ex);
                            
                            return false;
                        }
           
        }

    }
}  
        
        