

using System.Collections.Generic;
using UserAPI.Models;

namespace UserAPI.Services
{
    public interface IStudentService
    {
        List<Student> GetStudents();
        bool AddStudent(Student student);
        Student GetbyId(int id);
        bool UpStud(int id, Student st);
        bool DelStudent(int id);
        bool PartialUpStud(int id, Student st);
    }
}