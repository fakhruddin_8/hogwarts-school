using System;
using System.Collections.Generic;
using System.Linq;
using UserAPI.Models;

namespace UserAPI.Services
{
    public class TeacherService:ITeacherService
    {
        public UserContext _Teacherlist;
        
        public TeacherService(UserContext Teacherlist)
        {
            _Teacherlist=Teacherlist;
        }
        public List<Teacher> GetTeachers() 
       {  try
               {
                return _Teacherlist.Teachers.ToList();}
                catch(Exception ex)
                    {
                     throw ex;}
       }
        public bool AddTeacher(Teacher teacher)
        {   try{
                 _Teacherlist.Teachers.Add(teacher);
                _Teacherlist.SaveChanges();
                return true;}
                catch(Exception ex)
                {
                 Console.WriteLine(ex);
                 return false;
                }
        }   
        public Teacher GetbyId(int id)  
        {try{
                    var Teacher =_Teacherlist.Teachers.Find(id);
                    return Teacher;}
                 catch(Exception ex)
                    {
                    throw ex;}
        }  
        public bool UpTeacher(int id, Teacher tc)
        {try{
            var teacher =_Teacherlist.Teachers.Find(id);
               teacher.FirstName=tc.FirstName;
               teacher.LastName=tc.LastName;               
               teacher.Qualification=tc.Qualification;
               teacher.DateOfBirth=tc.DateOfBirth;
               teacher.Address=tc.Address;
               teacher.Experience=tc.Experience;
               teacher.ContactNumber=tc.ContactNumber;
               teacher.Gender=tc.Gender;
               _Teacherlist.SaveChanges();
             return true;}
             // _Stafflist.SaveChanges();}
             catch(Exception ex)
             {
              Console.WriteLine(ex);
              return false;
              }
        }

        public bool PartialUpdtTeacher(int id, Teacher tc)
        {try{ 
           var teacher =_Teacherlist.Teachers.Find(id);
            teacher.Address=tc.Address;
            teacher.Experience=tc.Experience;
            teacher.ContactNumber=tc.ContactNumber;
            teacher.Qualification=tc.Qualification;
            _Teacherlist.SaveChanges();
                    return true;}
                    catch(Exception ex)
                        {
                            Console.WriteLine(ex);
                            
                            return false;
                        }
        }
        public bool DelTeacher(int id)
        {try{
           var p=_Teacherlist.Teachers.Find(id);
            // p.FirstName = string.Empty;
            // p.LastName = string.Empty;
            // p.DateOfBirth = DateTime.MinValue;
            // p.ContactNumber = string.Empty;
            // p.Address = string.Empty;
            // p.Gender = string.Empty;
            // p.Qualification = string.Empty;
            // p.Experience = Convert.ToInt32(null);
              _Teacherlist.Remove(p);
              _Teacherlist.SaveChanges();
                    return true;}
                    catch(Exception ex)
                        {
                            Console.WriteLine(ex);
                            
                            return false;
                        }
        }

        public bool UplAssessmnt(Assessment assessment)
        { try{
                this._Teacherlist.Assessments.Add(assessment);
                 _Teacherlist.SaveChanges();
                return true;}
                catch(Exception ex)
                {
                 Console.WriteLine(ex);
                 return false;
                }
        }  
         public bool UploadResult(StudentResult studentResult)
        {try{
               _Teacherlist.StudentResults.Add(studentResult);
               _Teacherlist.SaveChanges();
                return true;}
                catch(Exception ex)
                {
                 Console.WriteLine(ex);
                 return false;
                }
        } 

    }
}