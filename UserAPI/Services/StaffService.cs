        using System;
        using System.Collections.Generic;
using System.Linq;
using UserAPI.Models;

        namespace UserAPI.Services
        {
                public class StaffService:IStaffService
            {
                public UserContext _Stafflist;
                public StaffService(UserContext Stafflist)
                {
                    _Stafflist=Stafflist;
                }
                
                public List<Staff> GetStaff() 
                {try
                    {
                        return _Stafflist.Staff.ToList();}
                    catch(Exception ex)
                        {
                        throw ex;}
                }
                public bool AddStaff(Staff staff)
                {try{
                    _Stafflist.Staff.Add(staff);
                    _Stafflist.SaveChanges();
                    return true;}
                    catch(Exception ex)
                    {
                        Console.WriteLine(ex);
                        return false;
                    }
                    
                }
                public Staff GetbyId(int id)  
                {try{
                    var staff =_Stafflist.Staff.Find(id);
                    return staff;}
                 catch(Exception ex)
                    {
                    throw ex;}
                }  
                public bool UpStaff(int id, Staff st)
                {try{

                        var staff =_Stafflist.Staff.Find(id);
                        staff.FirstName = st.Address;
                        staff.LastName = st.LastName;
                        staff.DateOfBirth = st.DateOfBirth;
                        staff.ContactNumber = st.ContactNumber;
                        staff.Address = st.Address;
                        staff.Role = st.Role;
                        _Stafflist.SaveChanges();
                        return true;}
                        // _Stafflist.SaveChanges();}
                        catch(Exception ex)
                        {
                            Console.WriteLine(ex);
                            
                            return false;
                        }
                    
                }

                public  bool PartialUpdtStaff(int id, Staff st)
                {try{
                    var Staff =_Stafflist.Staff.Find(id);
                    Staff.Address=st.Address;
                    Staff.ContactNumber=st.ContactNumber;
                    Staff.Role=st.Role;
                    _Stafflist.SaveChanges();
                    return true;}
                    catch(Exception ex)
                        {
                            Console.WriteLine(ex);
                            
                            return false;
                        }
                
                }
                public bool DelStaff(int id)
                {try{
                    var p=_Stafflist.Staff.Find(id);
                    //  p.FirstName = string.Empty;
                    //    p.LastName = string.Empty;
                    // p.DateOfBirth = DateTime.MinValue;
                    //    p.ContactNumber = string.Empty;
                    //   p.Address = string.Empty;
                    //   p.Role = string.Empty;
                    _Stafflist.Remove(p);
                    _Stafflist.SaveChanges();
                    return true;}
                    catch(Exception ex)
                        {
                            Console.WriteLine(ex);
                            
                            return false;
                        }
                }
            
                public bool AddProspect(StudentProspect studentProspect)
                {try{
                    TimeSpan difference = DateTime.Now.Subtract(studentProspect.DateOfBirth);
                    var days= difference.Days;
                    if((days/365)>=5)
                    {
                    _Stafflist.StudentProspects.Add(studentProspect);
                    _Stafflist.SaveChanges();
                    return true;}
                    
                    else
                        {
                            return false;
                        }}
                    
                    catch(Exception ex)
                        {
                            Console.WriteLine(ex);
                            
                            throw ex;
                        }
                    }
                
                List<StudentProspect> Actpros = new List<StudentProspect>();
                
                public List<StudentProspect> GetProspect()
                {
                    string status="Active";
                    foreach(var prospect in _Stafflist.StudentProspects)
                    {
                        if(prospect.Status==status)
                        {
                            
                            Actpros.Add( prospect);
                        }
                    }
                    
                    return Actpros;
                    
                }
            }
        }   
