using System.Collections.Generic;
using UserAPI.Models;

namespace UserAPI.Services
{
    public interface ITeacherService
    {
        List<Teacher> GetTeachers();
        bool AddTeacher(Teacher teacher);
        Teacher GetbyId(int id);
        bool UpTeacher(int id, Teacher tc);
        bool PartialUpdtTeacher(int id, Teacher tc);
        bool DelTeacher(int id);
        bool UplAssessmnt(Assessment assessment);
        bool UploadResult(StudentResult studentResult);
    }
}