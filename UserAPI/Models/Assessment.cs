using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserAPI.Models
{
    public class Assessment
    {
        [Key]
        public int AssessmentId { get; set; }

        public int TeacherId { get; set; }
        [ForeignKey("TeacherID")]
        

        [Required]
        [Column(TypeName="varchar(30)")]
        public string AssessmentName { get; set; }

        [Required]
        [Column(TypeName="varchar(200)")]
        public string AssessmentLink { get; set; }

        [Required]
        [Column(TypeName="date")]
        public DateTime DeadLine  { get; set; }
        public virtual Teacher Teacher  { get; set; }
    }
}