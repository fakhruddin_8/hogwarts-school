using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using UserAPI.Models;
using UserAPI.Services;

namespace UserAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class StaffController:ControllerBase
    {
        public IStaffService _IStaffService;
        public StaffController(IStaffService IStaffService)
    {
        this._IStaffService=IStaffService;
    }
       
        [HttpGet("AllStaff")]
        public ActionResult GetStaff()
        { 
             var d=this._IStaffService.GetStaff();
             if (d.Count==0)
            {
               return Ok("No Records");
            }else{return Ok(this._IStaffService.GetStaff());}
        }

        [HttpPost("AddStaff")]
        public ActionResult Post(Staff staff)
        { bool flag=this._IStaffService.AddStaff(staff);
        if(flag==true)
        {
            return Ok("Staff details Added");
        }else{
           return BadRequest("Please Give appropriate felds");
            }
            
        }
        
         [HttpGet("GetStaff/{id}")]
         public ActionResult Get(int id)
         {
            var staff =this._IStaffService.GetbyId(id);
            if (staff==null)
            {
               return Ok("No Records");
            }else{return Ok(this._IStaffService.GetbyId(id));}
            
         }
        
        [HttpPut("UpdateStaff/{id}")]
        public ActionResult Put(int id,Staff st)
        {
            bool flag= this._IStaffService.UpStaff(id,st);
            if (flag==true)
            {
               return Ok("Staff details Updated");
            }
            else{return NotFound("Staff Id not found");}
                           
        }

        [HttpPut("PartialUpdtStaff/{id}")]
        public ActionResult PutPar(int id,Staff st)
        {bool flag= this._IStaffService.PartialUpdtStaff(id,st);
            if (flag==true)
            {
               return Ok("Staff details Updated");
            }
            else{return NotFound("Staff Id not found");}              
        }

        [HttpDelete("RemoveStaff/{id}")]
        public ActionResult Delete(int id)
        {bool flag=this._IStaffService.DelStaff(id);
         if (flag==true)
            {
               return Ok("Staff details Removed");
            }
            else{return NotFound("Staff Id not found");} 

        }

        [HttpPost("AddProspect")]
        public ActionResult AddProspect(StudentProspect StudentProspect )
        {bool flag=this._IStaffService.AddProspect(StudentProspect);
             if (flag==true)
            {
               return Ok("Prospect details Added");
            }
            else{return BadRequest("Give Correct Fields, Age should be above 5");} 
            
            
        }

         [HttpGet("GetProspectStatus")]
         public ActionResult GetProspect()
         {try{
           
              var actpros =this._IStaffService.GetProspect();
              if(actpros.Count==0)
              {
                    return Ok("NO Active prospects");
              }
              else
              {
                    return Ok(this._IStaffService.GetProspect());
              }
             }
             catch(Exception ex)
             {Console.WriteLine(ex);
             throw ex;}
         }
    
    }
}