    using System;
    using Microsoft.AspNetCore.Mvc;
    using UserAPI.Models;
    using UserAPI.Services;

    namespace UserAPI.Controllers
    {
        [ApiController]
        [Route("[controller]")]
            
        public class TeacherController:ControllerBase
        {
            public ITeacherService _ITeacherService;
            public TeacherController(ITeacherService ITeacherService)
        {
            this._ITeacherService=ITeacherService;
        }
            

            [HttpGet("AllTeachers")]
            public ActionResult GetTeachers()
            {   var Teacher=this._ITeacherService.GetTeachers();
                if(Teacher.Count==0)
                {
                     return Ok("No Records");
                }
                else
                {
                    return Ok(this._ITeacherService.GetTeachers());
                }
            }

            [HttpPost("AddTeacher")]
            public ActionResult Post(Teacher teacher)
            {bool flag=this._ITeacherService.AddTeacher(teacher);
                if(flag==true)
                {
                    return Ok("Teacher Added");
                }else{
                return BadRequest("Please Give appropriate felds");
                    }
            }
            
            [HttpGet("GetTeacher/{id}")]
            public ActionResult Get(int id)
            {
            var staff =this._ITeacherService.GetbyId(id);
            if (staff==null)
            {
               return Ok("No Records");
            }else{return Ok(this._ITeacherService.GetbyId(id));}
            
            }
            
            [HttpPut("UpdateTeacher/{id}")]
            public ActionResult Put(int id,Teacher st)
            {
            bool flag=  this._ITeacherService.UpTeacher(id,st);
            if (flag==true)
            {
               return Ok("Teacher details Updated");
            }
            else{return NotFound("Teacher Id not found");}
                        
            }

            [HttpPut("PartialUpdtTeacher/{id}")]
            public ActionResult PutPar(int id,Teacher tc)
            {bool flag= this._ITeacherService.PartialUpdtTeacher(id,tc);
            if (flag==true)
            {
               return Ok("Teacher details Updated");
            }
            else{return NotFound("Teacher Id not found");} 
            }

            [HttpDelete("RemoveTeacher/{id}")]
            public ActionResult Delete(int id)
            {bool flag=this._ITeacherService.DelTeacher(id);
             if (flag==true)
            {
               return Ok("Teacher details Removed");
            }
            else{return NotFound("Teacher Id not found");}
            } 

            [HttpPost("UploadAssessment")]
            public ActionResult PostAssessment(Assessment assessment)
            {bool flag=this._ITeacherService.UplAssessmnt(assessment);
                if(flag==true)
                {
                    return Ok("Assessment Added");
                }else{
                return BadRequest("Please Give appropriate felds");
                }
            } 

            [HttpPost("UploadResult")]
            public ActionResult PostResult(StudentResult studentResult)
            {bool flag=this._ITeacherService.UploadResult(studentResult);
                if(flag==true)
                {
                    return Ok("Result Uploaded");
                }else{
                return BadRequest("Please Give appropriate felds");
                }
            }
        }
        
    }