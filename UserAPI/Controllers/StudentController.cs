    using System;
    using CourseAPI.Services;
    using Microsoft.AspNetCore.Mvc;
    using UserAPI.Models;
    using UserAPI.Services;

    namespace UserAPI.Controllers
    {
        [ApiController]
        [Route("[controller]")]
        public class StudentController:ControllerBase
        {
            public ICourseService _ICourseService;
            public IStudentService _IStudentService;
            public StudentController(ICourseService ICourseService,IStudentService IStudentService)
        {
            this._ICourseService =ICourseService;
            _IStudentService=IStudentService;
        }
            [HttpGet("Courses")]
            public ActionResult Get()
            { var Course=this._ICourseService.GetCourses();
              if(Course.Count==0)
                {
                     return Ok("No Records");
                }
                else
                {
                    return Ok(this._ICourseService.GetCourses());
                }
            }

            [HttpGet("AllStudents")]
            public ActionResult GetAllStudents()
            {if(this._IStudentService.GetStudents()==null)
                {
                     return Ok("No Records");
                }
                else
                {
                    return Ok(this._IStudentService.GetStudents());
                }
            }

            [HttpPost("AddStudent")]
            public ActionResult Post(Student student)
            {bool flag=this._IStudentService.AddStudent(student);
                if(flag==true)
                {
                    return Ok(" Student Added");
                }else{
                return BadRequest("Please Give appropriate felds");
                    }
                    
                }
            
            [HttpGet("GetStudent/{id}")]
            public ActionResult Get(int id)
            {
            var staff =this._IStudentService.GetbyId(id);
            if (staff==null)
            {
               return Ok("No Records");
            }else{return Ok(this._IStudentService.GetbyId(id));}
            
            }
            
            [HttpPut("UpdateStudent/{id}")]
            public ActionResult Put(int id,Student st)
            {
            bool flag= this._IStudentService.UpStud(id,st);
            if (flag==true)
            {
               return Ok("Student details Updated");
            }
            else{return NotFound("Student Id not found");}
                  
            }

            [HttpPut("PartialUpdtStud/{id}")]
            public ActionResult PutPar(int id,Student st)
            {bool flag= this._IStudentService.PartialUpStud(id,st);
            if (flag==true)
            {
               return Ok("Student details Updated");
            }
            else{return NotFound("Student Id not found");} 
                   
            }

            [HttpDelete("RemoveStudent/{id}")]
            public ActionResult Delete(int id)
            {bool flag=this._IStudentService.DelStudent(id);
             if (flag==true)
            {
               return Ok("Student Removed");
            }
            else{return NotFound("Student Id not found");}
            }
        
        }
    }